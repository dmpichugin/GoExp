package bstree

import (
	"bytes"
	"errors"
	"fmt"
)

type Element struct {
	Data   int
	Parent *Element
	Left   *Element
	Right  *Element
}

type BSTree struct {
	head  *Element
	count int
}

func New() *BSTree {
	b := new(BSTree)
	return b
}

func (b *BSTree) Push(val int) (*BSTree, error) {
	e := &Element{val, nil, nil, nil}
	if b.count == 0 {
		b.head = e
		b.count++
		return b, nil
	}

	t := b.head
	notcommen := true
	// Идем по дереву и находим куда вставлять
	for notcommen {
		if val > t.Data {
			if t.Right != nil {
				t = t.Right
			} else {
				notcommen = false
			}
		} else {
			if t.Left != nil {
				t = t.Left
			} else {
				notcommen = false
			}
		}
	}
	// Если узел с таким значением существует
	if val == t.Data {
		return b, errors.New("Value already exists")
	}
	if val > t.Data {
		t.Right = e
	} else {
		t.Left = e
	}
	e.Parent = t

	b.count++
	return b, nil
}

func (b *BSTree) Len() int {
	return b.count
}

func (b *BSTree) Show() {
	var buf bytes.Buffer
	inOrderTravel(b.head, &buf)
	fmt.Println(buf.String())
}

func inOrderTravel(e *Element, buf *bytes.Buffer) {
	if e != nil {
		inOrderTravel(e.Left, buf)
		fmt.Fprintf(buf, "%d ", e.Data)
		inOrderTravel(e.Right, buf)
	}
}

// Вывод по уровням
func (b *BSTree) ShowByLevel() {
	a := make(map[int][]int)
	addlevel(b.head, a, 0)

	var buf bytes.Buffer
	for i := 0; i < len(a); i++ {
		fmt.Fprintf(&buf, "Level %d: ", i)
		for _, r := range a[i] {
			fmt.Fprintf(&buf, "%d ", r)
		}
		buf.WriteByte('\n')
	}
	fmt.Print(buf.String())
}

func addlevel(e *Element, m map[int][]int, level int) {
	if e != nil {
		m[level] = append(m[level], e.Data)
		addlevel(e.Left, m, level+1)
		addlevel(e.Right, m, level+1)
	}
}

func (b *BSTree) HasInTree(val int) bool {
	if b.count == 0 {
		return false
	}
	p := b.head
	for p != nil {
		if val > p.Data {
			p = p.Right
		} else if val < p.Data {
			p = p.Left
		} else {
			return true
		}
	}
	return false
}

func (b *BSTree) Remove(val int) error {
	if b.count == 0 {
		return errors.New("Tree don't have element with this value")
	}
	p := b.head
	// Находим нужный элемент
	for p != nil && p.Data != val {
		if val > p.Data {
			p = p.Right
		} else if val < p.Data {
			p = p.Left
		}
		if p == nil {
			return errors.New("Tree don't have element with this value")
		}
	}
	// Если у элемента нет дочерних узлов, то удялем на него ссылку и родителя.
	if p.Left == nil && p.Right == nil {

		parent := p.Parent
		p.Parent = nil
		if p.Data < parent.Data {
			parent.Left = nil
		} else {
			parent.Right = nil
		}
		b.count--
		return nil
	}
	// Если есть один дочерний узел
	if p.Left == nil || p.Right == nil {
		parent := p.Parent
		var child *Element
		if p.Left != nil {
			child = p.Left
		} else {
			child = p.Right
		}
		// Родитель элемента указывает на дочерний узел элемента
		if p.Data < parent.Data {
			parent.Left = child
		} else {
			parent.Right = child
		}
		child.Parent = parent
		p.Parent, p.Right, p.Left = nil, nil, nil
		b.count--
		return nil
	}
	// Наконец, если у узла два дочерних узла,
	// то нужно найти следующий за ним элемент
	// (у этого элемента не будет левого потомка),
	// его правого потомка подвесить на место найденного элемента,
	// а удаляемый узел заменить найденным узлом.
	// Таким образом, свойство бинарного дерева поиска не будет нарушено.
	if p.Left != nil && p.Right != nil {
		p2 := p.Right
		for p2.Left != nil {
			p2 = p2.Left
		}
		// Подвешиваем правого потомка на место найденного элемента
		p2.Parent.Left = p2.Right
		if p2.Right != nil {
			p2.Right.Parent = p2.Parent
		}
		// Заменяем удаляемый узел найденным узлом
		p.Data = p2.Data
		p2.Right, p2.Parent = nil, nil

		b.count--
		return nil
	}
	return errors.New("Tree don't have element with this value")

}
