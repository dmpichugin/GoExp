package main

import (
	"bufio"
	"fmt"
	"net"
)

func main() {
	// Начинаем слушать 7788 порт
	server, err := net.Listen("tcp", ":7788")
	if server == nil {
		panic("Error start server: " + err.Error())
	}
	fmt.Println("Server started on 7788 port")
	ch := make(chan net.Conn)
	go func() {
		// Принимаем соединения
		for {
			client, err := server.Accept()
			if client == nil {
				fmt.Printf("Error accept: " + err.Error())
				continue
			}
			ch <- client
		}
	}()

	for {
		go handleConn(<-ch)
	}
}

func handleConn(client net.Conn) {
	b := bufio.NewReader(client)
	for {
		line, err := b.ReadBytes('\n')
		if err != nil {
			break
		}
		client.Write(line)
	}
}
