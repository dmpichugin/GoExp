package main

import (
	"net/http"

	"github.com/labstack/echo"
)

func AddBook(c echo.Context) error {
	b := &Book{}
	var err error
	if err = c.Bind(b); err != nil {
		return c.JSON(http.StatusUnsupportedMediaType, &DefaultResponse{
			Success: false,
			Message: "Set content-type application/json or check your payload data",
		})
	}
	err = AddBookInDB(b)
	if err != nil {
		return c.JSON(http.StatusUnsupportedMediaType, &DefaultResponse{
			Success: false,
			Message: "Book is not added",
		})
	}
	return c.JSON(http.StatusOK, &DefaultResponse{
		Success: true,
		Message: "Added",
	})
}

func GetBook(c echo.Context) error {
	id := c.Param("id")
	b, err := GetBookFromDB(id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, &DefaultResponse{
			Success: false,
			Message: "Book not found",
		})
	}
	return c.JSON(http.StatusOK, &BookResponse{
		Success: true,
		Message: "Found",
		Book:    b,
	})
}

func DeleteBook(c echo.Context) error {
	id := c.Param("id")
	err := DeleteBookInDB(id)
	if err != nil {
		return c.JSON(http.StatusUnsupportedMediaType, &DefaultResponse{
			Success: false,
			Message: "Book is not found",
		})
	}
	return c.JSON(http.StatusOK, &DefaultResponse{
		Success: true,
		Message: "Removed",
	})
}

func UpdateBook(c echo.Context) error {
	id := c.Param("id")
	b := &Book{}
	var err error
	if err = c.Bind(b); err != nil {
		return c.JSON(http.StatusUnsupportedMediaType, &DefaultResponse{
			Success: false,
			Message: "Set content-type application/json or check your payload data",
		})
	}
	if id != b.Isbn {
		return c.JSON(http.StatusUnsupportedMediaType, &DefaultResponse{
			Success: false,
			Message: "Book is not update. Id not equal isbn in JSON",
		})
	}
	err = UpdateBookiInDB(b)
	if err != nil {
		return c.JSON(http.StatusUnsupportedMediaType, &DefaultResponse{
			Success: false,
			Message: "Book is not updated",
		})
	}
	return c.JSON(http.StatusOK, &DefaultResponse{
		Success: true,
		Message: "updated",
	})
}
