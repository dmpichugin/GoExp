package main

type (
	DefaultResponse struct {
		Success bool   `json:"success"`
		Message string `json:"message"`
	}

	Book struct {
		Isbn   string  `json:"isbn"`
		Title  string  `json:"title"`
		Author string  `json:"author"`
		Price  float64 `json:"price"`
	}

	BookResponse struct {
		Success bool   `json:"success"`
		Message string `json:"message"`
		Book    *Book  `json:"book"`
	}
)
