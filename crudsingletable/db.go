package main

import (
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/lib/pq"
	"log"
)

func GetAllBooksFromDB() []*Book {
	rows, err := db.Query("SELECT * FROM books")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	bks := make([]*Book, 0)
	for rows.Next() {
		bk := &Book{}
		err = rows.Scan(&bk.Isbn, &bk.Title, &bk.Author, &bk.Price)
		if err != nil {
			log.Fatal(err)
		}
		bks = append(bks, bk)
	}
	if err = rows.Err(); err != nil {
		log.Fatal(err)
	}
	return bks
}

func GetBookFromDB(isbn string) (*Book, error) {
	row := db.QueryRow("SELECT * FROM books WHERE isbn = $1", isbn)

	bk := &Book{}
	err := row.Scan(&bk.Isbn, &bk.Title, &bk.Author, &bk.Price)
	if err == sql.ErrNoRows {
		return bk, err
	}
	return bk, nil
}

func AddBookInDB(b *Book) error {
	var err error
	if b.Author == "" || b.Isbn == "" || b.Price == 0.0 || b.Title == "" {
		return errors.New("Some fields is emphty")
	}
	result, err := db.Exec("INSERT INTO books VALUES($1, $2, $3, $4)", b.Isbn, b.Title, b.Author, b.Price)
	if err != nil {
		return err
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}
	fmt.Printf("Book %s created successfully (%d row affected)\n", b.Isbn, rowsAffected)
	return nil
}

func DeleteBookInDB(s string) error {
	result, err := db.Exec("DELETE FROM books WHERE isbn = $1", s)
	if err != nil {
		return err
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}
	fmt.Printf("Book %s deleted successfully (%d row affected)\n", s, rowsAffected)
	return nil
}

func UpdateBookiInDB(b *Book) error {
	result, err := db.Exec("UPDATE books SET title=$2,author=$3,price=$4 WHERE isbn=$1", b.Isbn, b.Title, b.Author, b.Price)
	if err != nil {
		fmt.Println(err)
		return err
	}
	log.Println("db2")
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}
	fmt.Printf("Book %s update successfully (%d row affected)\n", b.Isbn, rowsAffected)
	return nil
}
