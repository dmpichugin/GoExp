package main

import (
	"database/sql"
	"fmt"
	"github.com/labstack/echo"
	_ "github.com/lib/pq"
	"log"
)

var db *sql.DB

const (
	user       = "postgres"
	port       = "1234"
	table      = "books"
	ssldisable = "?sslmode=disable"
)

func main() {
	var err error
	db, err = sql.Open("postgres", fmt.Sprintf("postgres://%s:%s@localhost/%s%s", user, port, table, ssldisable))
	if err != nil {
		log.Fatal(err)
	}
	if err = db.Ping(); err != nil {
		log.Fatal(err)
	}

	e := echo.New()
	g := e.Group("/api")
	g.POST("/books", AddBook)
	g.GET("/books/:id", GetBook)
	g.PUT("/books/:id", UpdateBook)
	g.DELETE("/books/:id", DeleteBook)
	log.Fatal(e.Start(":7777"))
}
