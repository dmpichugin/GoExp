CREATE TABLE public.books
(
  isbn character(14) NOT NULL,
  title character varying(255) NOT NULL,
  author character varying(225) NOT NULL,
  price numeric(5,2) NOT NULL,
  CONSTRAINT pmkey_books PRIMARY KEY (isbn)
)