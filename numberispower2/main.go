package main

import (
	"fmt"
)

func main() {
	fmt.Println(numberispow2(0))
	fmt.Println(numberispow2(1))
	fmt.Println(numberispow2(-2))
	fmt.Println(numberispow2(2))
	fmt.Println(numberispow2(4))
	fmt.Println(numberispow2(8))
	fmt.Println(numberispow2(9))
}

func numberispow2(num int) bool {
	return (num > 0) && ((num & (num - 1)) == 0)
}
