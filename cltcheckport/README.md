About

Write command line tool for network exploration. It displays a list of open, closed and timeout(optional) TCP ports for given target. Programming language: Go

Description
command target -p(--ports) list of comma separated ports  -v(--verbose)

target - required, IP address (127.0.0.1) or domain (example.com)
-p - ports delimiter is a comma, ranges are also supported, example: command example.com -p 80,81,1024-2048,9042. Frist 1024 ports should be scanned if not specified 
-v  includes information about timeout ports 

examples of usage:

```
$ command example.com
$ command 192.168.1.42 -p 80
$ command 192.168.1.42 -v -p 22,8500,9042,80-92
```

output:

```
PORT State
22   timeout
80   open
81   closed
82   closed
8500 open
9042 closed
```

Please provide a GitHub(GitLab, BitBucket) repository with information about tool, how to install and run it

programming would be nice to see, support for multiple targets with ranges (command example.com, golang.org, 192.168.0.0/16 -p 80)