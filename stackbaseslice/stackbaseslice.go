package stackbaseslice

import (
	"errors"
	"fmt"
)

type Stack struct {
	Data  []int
	Count int
}

func New() *Stack {
	s := new(Stack)
	return s
}

func (s *Stack) Push(val int) *Stack {
	s.Data = append(s.Data, val)
	s.Count++
	return s
}

func (s *Stack) Show() {
	if len(s.Data) == 0 {
		fmt.Println("Stack emphty")
	} else {
		fmt.Println(s.Data)
	}
}

func (s *Stack) Pop() (int, error) {
	if len(s.Data) == 0 {
		return 0, errors.New("Stack is epmhty")
	}
	v := s.Data[len(s.Data)-1]
	s.Data = s.Data[:len(s.Data)-1]
	s.Count--
	return v, nil
}
