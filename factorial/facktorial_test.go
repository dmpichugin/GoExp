package factorial

import (
	"testing"
)

func TestFactorial(t *testing.T) {
	var tests = []struct {
		input int64
		want  int64
	}{
		{0, 0},
		{1, 1},
		{2, 2},
		{3, 6},
		{4, 24},
		{5, 120},
		{6, 720},
		{7, 5040},
		{8, 40320},
		{9, 362880},
		{10, 3628800},
		{11, 39916800},
		{12, 479001600},
		{13, 6227020800},
		{14, 87178291200},
		{15, 1307674368000},
	}
	for _, test := range tests {
		if got := Factorial(test.input); got != test.want {
			t.Errorf("Factorial(%d) = %d", test.input, got)
		}
	}
}

func TestFactorialRec(t *testing.T) {
	var tests = []struct {
		input int64
		want  int64
	}{
		{0, 0},
		{1, 1},
		{2, 2},
		{3, 6},
		{4, 24},
		{5, 120},
		{6, 720},
		{7, 5040},
		{8, 40320},
		{9, 362880},
		{10, 3628800},
		{11, 39916800},
		{12, 479001600},
		{13, 6227020800},
		{14, 87178291200},
		{15, 1307674368000},
	}
	for _, test := range tests {
		if got := FactorialRec(test.input); got != test.want {
			t.Errorf("FactorialRec(%d) = %d", test.input, got)
		}
	}
}

func BenchmarkFactorial5(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Factorial(5)
	}
}

func BenchmarkFactorial10(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Factorial(10)
	}
}

func BenchmarkFactorial15(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Factorial(15)
	}
}

func BenchmarkFactoriaRecl5(b *testing.B) {
	for i := 0; i < b.N; i++ {
		FactorialRec(5)
	}
}

func BenchmarkFactorialRec10(b *testing.B) {
	for i := 0; i < b.N; i++ {
		FactorialRec(10)
	}
}

func BenchmarkFactorialRec15(b *testing.B) {
	for i := 0; i < b.N; i++ {
		FactorialRec(15)
	}
}
