package factorial

func Factorial(num int64) int64 {
	if num < 1 {
		return 0
	}
	var result int64 = 1
	var i int64 = 1

	for i = 1; i < num+1; i++ {
		result *= i
	}
	return result
}

func FactorialRec(num int64) int64 {
	if num == 0 || num == 1 {
		return num
	}
	return num * FactorialRec(num-1)
}
