package sortselection

func SortSelection(a []int) {
	for i := 0; i < len(a)-1; i++ {
		min_index := i
		for j := i + 1; j < len(a); j++ {
			if a[j] < a[min_index] {
				min_index = j
			}
		}
		if min_index != i {
			a[i], a[min_index] = a[min_index], a[i]
		}
	}
}
