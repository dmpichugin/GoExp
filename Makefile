test:
	go test -count=1 ./bsearch
	go test -count=1 ./doublylist
	go test -count=1 ./list
	go test -count=1 ./sortbuble
	go test -count=1 ./sortinsertion
	go test -count=1 ./sortselection
	go test -count=1 ./sortmerge
	go test -count=1 ./stackbaseslice
	go test -count=1 ./fibonaci
	go test -count=1 ./sorthoar
	go test -count=1 ./checksum
	go test -count=1 ./factorial
	