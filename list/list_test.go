package list

import (
	"testing"
)

func TestAddPopElem(t *testing.T) {
	a := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}
	b := []int{}
	l := New()
	for _, v := range a {
		l.Push(v)
	}
	for l.Len() != 0 {
		v, _ := l.Pop()
		b = append(b, v)
	}
	if len(a) != len(b) {
		t.Error("Lens are not equal")
	}
	for k, v := range a {
		if b[len(b)-1-k] != v {
			t.Error("Value not equal")
		}
	}
}

func BenchmarkAddElem(b *testing.B) {
	l := New()
	for i := 0; i < b.N; i++ {
		l.Push(i)
	}
}
