package list

import (
	"bytes"
	"errors"
	"fmt"
)

type Element struct {
	Data int
	Next *Element
}

type List struct {
	head  *Element
	last  *Element
	count int
}

func New() *List {
	l := new(List)
	return l
}

func (l *List) Len() int {
	return l.count
}

func (l *List) Push(val int) *List {
	v := &Element{val, nil}
	if l.head == nil {
		l.head = v
		l.last = v
	} else {
		l.last.Next = v
		l.last = v
	}
	l.count++
	return l
}

func (l *List) Clear() {
	if l.Len() == 0 {
		return
	} else {
		var p1, p2 *Element
		p1 = l.head
		if p1.Next == nil { // если 1 элемент
			l.head = nil
			l.last = nil
		} else {
			for p1 := l.head; p1 != nil; {
				p2 = p1
				p1 = p1.Next
				p2.Next = nil
				l.count--
			}
			l.head = nil
			l.last = nil
		}
	}
}

func (l *List) Pop() (int, error) {
	if l.Len() == 0 {
		return 0, errors.New("List is emphty")
	}
	var p1, p2 *Element

	for p1 = l.head; p1.Next != nil; p1 = p1.Next {
		p2 = p1
	}
	l.last = p2
	if l.count > 1 {
		p2.Next = nil
	}
	l.count--
	return p1.Data, nil
}

func (l *List) PushBegin(val int) *List {
	v := &Element{val, nil}
	if l.head == nil {
		l.head = v
		l.last = v
	} else {
		v.Next = l.head
		l.head = v
	}
	l.count++
	return l
}

func (l *List) PopBegin() (int, error) {
	if l.Len() == 0 {
		return 0, errors.New("List is emphty")
	}
	var p1 *Element

	p1 = l.head
	l.head = p1.Next
	p1.Next = nil
	l.count--
	return p1.Data, nil
}

func (l *List) Show() {
	if l.Len() == 0 {
		fmt.Println("[ ]")
	} else {
		var buf bytes.Buffer
		buf.WriteString("[ ")
		for p := l.head; p != nil; p = p.Next {
			fmt.Fprintf(&buf, "%d ", p.Data)
			// fmt.Printf("%d ", p.Data)
		}
		buf.WriteByte(']')
		fmt.Println(buf.String())
	}
}
