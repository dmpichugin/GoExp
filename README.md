[![Build Status](https://travis-ci.org/dmitrypich/golangexp.svg?branch=master)](https://travis-ci.org/dmitrypich/golangexp)

Репозиторий для опыта.

Список:
- Сортировки
	+ Сортировка пузырьком (bubble sort)
	+ Сортировка слиянием (merge sort)
	+ Сортировка вставками (Insertion sort)
	+ Сортировка выбором (Selection sort)
	+ Быстраяя сортировка(Хоара) (quicksort Hoare)
- Структуры данных
	+ Однонаправленный список (Singly linked list)
	+ Двунаправленный список (Doubly linked list)
	+ Стэк(на слайсах) (stack)
	+ Бинарное дерево поиска (binary tree)
- Алгоритмы
	+ Бинарный поиск(правосторонний) (binary search)
	+ Числа Фибоначчи (Fibonacci number)
	+ Вычисление факториала (factorial)
- Простые задачи
	+ Перевод ip из числа в строку
	+ Является ли число степенью двойки
- Разные программы
	+ Эхо сервер на tcp (echo server on tcp)
	+ Простой CRUD для одной таблицы(на базе echo framework) (simple CRUD for one table)
	+ Решение для Highloadcup(не для высоких нагрузок)
	+ Тестовое задание mailru(найти число вхождений строки в файл) (test task mailru)
	+ Программа для подсчета и проверки хешей файлов
