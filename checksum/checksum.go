package main

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"flag"
	"fmt"
	"hash"
	"io"
	"log"
	"os"
	"strings"
)

var (
	typeHash    = flag.String("t", "md5", "type hashfunc. support md5,sha1,sha256,sha384,sha512")
	userHashSum = flag.String("c", "", "hash value for comparison")
	sizeHash    = flag.String("s", "high", "strings size. high or low")
)

func main() {
	flag.Parse()
	if *sizeHash != "low" && *sizeHash != "high" {
		log.Fatal("Incorrect size")
	}
	if flag.NArg() < 1 {
		fmt.Fprintf(os.Stderr, "")
		os.Exit(1)
	}
	pathFile := &flag.Args()[0]

	res, err := CheckSum(*pathFile, *typeHash, *userHashSum, *sizeHash)
	if err != nil {
		log.Fatal(err)
	}
	// Если нету строки для сравнения, то надо вывести хеш файла
	if *userHashSum != "" {
		if res {
			fmt.Println("Hash match")
		} else {
			fmt.Println("Hash not match")
		}
	}
}

// CheckSum calculates the hashsum of the given file. If the user's sum is set compares with the hashsum of the file
func CheckSum(pathFile, typeHash, userSum, sizeHash string) (bool, error) {
	f, err := os.Open(pathFile)
	if err != nil {
		return false, err
	}
	defer f.Close()

	var hasher hash.Hash
	switch strings.ToLower(typeHash) {
	case "md5":
		hasher = md5.New()
	case "sha1":
		hasher = sha1.New()
	case "sha256":
		hasher = sha256.New()
	case "sha384":
		hasher = sha512.New384()
	case "sha512":
		hasher = sha512.New()
	default:
		return false, fmt.Errorf("Broken hash")
	}
	if _, err := io.Copy(hasher, f); err != nil {
		return false, err
	}

	if userSum != "" {
		if strings.ToUpper(userSum) == fmt.Sprintf("%X", hasher.Sum(nil)) {
			return true, nil
		}
		return false, nil
	}
	if sizeHash == "high" {
		fmt.Printf("%X", hasher.Sum(nil))
	} else if sizeHash == "low" {
		fmt.Printf("%x", hasher.Sum(nil))
	}
	return true, nil
}
