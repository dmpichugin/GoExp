package main

import (
	"testing"
)

func TestCheckSumEqual(t *testing.T) {
	size := "high"
	var tests = []struct {
		pathFile    string
		typeHash    string
		userHashSum string
		result      bool
	}{
		{"testdata/1.jpg", "md5", "089CF48926064112A30317380AC84B12", true},
		{"testdata/1.jpg", "sha1", "0852BECD7E76CD6F3D317DF3A90AA1745ED273F4", true},
		{"testdata/1.jpg", "sha256", "F769239201CF6D40FF7CE44A432FB46E0E62CD522641CE30A4BDE59F035EFF2C", true},
		{"testdata/1.jpg", "sha384", "0664A89ADC3617070507B366519FAE272A86C464E1FC5538B37DB780465502828144E9999958722E7903BA157605BE63", true},
		{"testdata/1.jpg", "sha512", "ECB4ED5C23CA45C7AFD3881E541505041EB0C74686DDC12BAE063298B9E742B65339A5743C344822B3A253A3946ABDCE2F12758E555F0E8B0FE3C5A990CC98A2", true},
		{"testdata/1.jpg", "md5", "089CF48926064112A30317380AC84Bfa", false},
		{"testdata/1.jpg", "md", "089CF48926064112A30317380AC84Bfa", false},
		{"testdata/2.txt", "md5", "86FB269D190D2C85F6E0468CECA42A20", true},
		{"testdata/2.txt", "sha1", "D3486AE9136E7856BC42212385EA797094475802", true},
		{"testdata/2.txt", "sha256", "C0535E4BE2B79FFD93291305436BF889314E4A3FAEC05ECFFCBB7DF31AD9E51A", true},
		{"testdata/2.txt", "sha384", "86255FA2C36E4B30969EAE17DC34C772CBEBDFC58B58403900BE87614EB1A34B8780263F255EB5E65CA9BBB8641CCCFE", true},
		{"testdata/2.txt", "sha512", "F6CDE2A0F819314CDDE55FC227D8D7DAE3D28CC556222A0A8AD66D91CCAD4AAD6094F517A2182360C9AACF6A3DC323162CB6FD8CDFFEDB0FE038F55E85FFB5B6", true},
		{"testdata/2.txt", "md5", "089CF48926064112A30317380AC84Bfa", false},
		{"testdata/2.txt", "md", "089CF48926064112A30317380AC84Bfa", false},
	}
	for _, test := range tests {
		if got, _ := CheckSum(test.pathFile, test.typeHash, test.userHashSum, size); got != test.result {
			t.Errorf("pathFile = \"%s\", typeHash=\"%s\", userHashSum =%s returned %t!=%t want", test.pathFile, test.typeHash, test.userHashSum, got, test.result)
		}
	}
}
