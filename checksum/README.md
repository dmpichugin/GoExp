checksum проверяет файл и возвращает контрольную сумму для md5, sha1 и sha2 (sha256, sha384 и sha512).

Примеры работы:
```console
$ checksum -f=README.md
AD2098BFD50DD9A6F98C2AB5DA8E8661
```
```console
$ checksum -f=README.md -t=sha1
9AF3EA8B63CBF2E327937985142D6E324DCF15D8
```
```console
$ checksum -f=README.md -t=sha256
D0E3A04889206A2DB0BC6AB831BD95BC59A2D6FBED5E09537022D67483DA1C1C
```
```console
$ checksum -f=README.md -t=sha1 -c=9AF3EA8B63CBF2E327937985142D6E324DCF15D8
Hashes match.
```
```console
$ ./checksum -f=README.md -t=sha1 -c=9AF3EA8B63CBF2E327937985142D6E324DCF15D8
Hash not match
```
