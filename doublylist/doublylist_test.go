package doublylist

import (
	"testing"
)

func TestAddPopElem(t *testing.T) {
	a := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}
	b := []int{}
	d := New()
	for _, v := range a {
		d.PushLeft(v)
	}
	for d.Len() != 0 {
		v, _ := d.PopLeft()
		b = append(b, v)
	}
	if len(a) != len(b) {
		t.Error("Lens are not equal")
	}
	for k, v := range a {
		if b[len(b)-1-k] != v {
			t.Error("Value not equal")
		}
	}
}
