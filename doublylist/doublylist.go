package doublylist

import (
	"bytes"
	"errors"
	"fmt"
)

type Element struct {
	Data int
	Prev *Element
	Next *Element
}

type DList struct {
	left  *Element
	right *Element
	count int
}

func New() *DList {
	d := new(DList)
	return d
}

func (d *DList) Len() int {
	return d.count
}

func (d *DList) Show() {
	if d.Len() == 0 {
		fmt.Println("Len:", d.Len())
		fmt.Println("[ ]")
	} else {
		fmt.Println("Len:", d.Len())
		var buf bytes.Buffer
		buf.WriteString("[ ")
		if d.Len() == 1 {
			fmt.Fprintf(&buf, "%d ", d.left.Data)
		} else {
			for p := d.left; p != nil; p = p.Next {
				fmt.Fprintf(&buf, "%d ", p.Data)
			}
		}
		buf.WriteByte(']')
		fmt.Println(buf.String())
	}
}

func (d *DList) PushLeft(val int) *DList {
	v := &Element{val, nil, nil}
	if d.Len() == 0 {
		d.left = v
		d.right = v
	} else {
		d.left.Prev = v
		v.Next = d.left
		d.left = v
	}
	d.count++
	return d
}

func (d *DList) PushRight(val int) *DList {
	v := &Element{val, nil, nil}
	if d.Len() == 0 {
		d.left = v
		d.right = v
	} else {
		v.Prev = d.right
		d.right.Next = v
		d.right = v
	}
	d.count++
	return d
}

func (d *DList) Clear() *DList {
	var q *Element
	for p := d.left; p != nil; {
		q = p
		p = p.Next
		q.Next = nil
		q.Prev = nil
		d.count--
	}
	d.left, d.right = nil, nil
	return d
}

func (d *DList) PopLeft() (int, error) {
	var result int
	if d.Len() == 0 {
		return -1, errors.New("DList is empty")
	} else if d.Len() == 1 {
		result = d.left.Data
		d.count--
		d.left, d.right = nil, nil
	} else {
		result = d.left.Data
		d.count--
		d.left, d.left.Next, d.left.Next.Prev = d.left.Next, nil, nil
		d.left.Prev = nil
	}
	return result, nil
}

func (d *DList) PopRight() (int, error) {
	var result int
	if d.Len() == 0 {
		return -1, errors.New("DList is empty")
	} else if d.Len() == 1 {
		result = d.right.Data
		d.count--
		d.left, d.right = nil, nil
	} else {
		result = d.right.Data
		d.count--
		d.right, d.right.Prev, d.right.Prev.Next = d.right.Prev, nil, nil
		d.right.Next = nil
	}
	return result, nil
}
