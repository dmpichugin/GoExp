package main

import (
	"github.com/labstack/echo"
	"net/http"
)

func APIGetByShorUrl(c echo.Context) error {
	shortUrl := c.Param("shortUrl")
	longUrl, err := DBGetLongUrl(shortUrl)
	if err != nil {
		return c.NoContent(http.StatusNotFound)
	}
	return c.Redirect(http.StatusPermanentRedirect, longUrl)
}

func APIAddLongUrl(c echo.Context) error {
	formParams, err := c.FormParams()
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	longUrl := formParams.Get("lUrl")
	shortUrl, err := DBAddUrl(longUrl)
	if err != nil {
		return c.NoContent(http.StatusNotFound)
	}

	return c.String(http.StatusOK, shortUrl)
}
