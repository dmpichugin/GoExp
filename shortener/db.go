package main

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
)

func DBGetLongUrl(shortUrl string) (string, error) {
	stm, err := db.Prepare("select long_url from urls where short_url = $1")
	if err != nil {
		fmt.Println(err)
		return "", err
	}

	result := stm.QueryRow(shortUrl)
	var longUrl string
	result.Scan(&longUrl)
	return longUrl, nil
}

func DBAddUrl(longUrl string) (string, error) {
	h := md5.New()
	h.Write([]byte(longUrl))
	hash := hex.EncodeToString(h.Sum(nil))

	stm, err := db.Prepare("Insert Into urls (long_url,short_url) values ($1,$2)")
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	_, err = stm.Exec(longUrl, hash)

	if err != nil {
		fmt.Println(err)
		return "", err
	}

	return hash, nil
}
