package main

import (
	"bufio"
	"flag"
	"fmt"
	"net/http"
	"os"
	"strings"
	"sync"
)

type ResultCheck struct {
	path     string
	pathType string
	count    int
	err      error
}

const k int = 10

func main() {
	flagType := flag.String("type", "url", "url or file")
	flag.Parse()
	pipe := make(chan ResultCheck)

	if *flagType != "file" && *flagType != "url" {
		fmt.Println("Invalid type")
		os.Exit(1)
	}

	go launcher(pipe, *flagType)

	var sumcount int
	for m := range pipe {
		if m.err == nil {
			fmt.Printf("count for %s %s : %d\n", m.pathType, m.path, m.count)
			sumcount += m.count
		}
	}
	fmt.Println("Total count:", sumcount)
}

// Считывает имена файлов и передает функции подсчета
func launcher(ch chan ResultCheck, TypeData string) {
	scaner := bufio.NewScanner(os.Stdin)
	semaphor := make(chan int, k)
	var wg sync.WaitGroup
	for scaner.Scan() {
		semaphor <- 1
		wg.Add(1)
		go func(path string) {
			ch <- counting(path, TypeData)
			<-semaphor
			wg.Done()
		}(scaner.Text())
	}
	wg.Wait()
	close(ch)
}

// Функция подсчета строк "Go" в файле
func counting(path, pathType string) ResultCheck {
	result := ResultCheck{path, pathType, 0, nil}
	var scaner *bufio.Scanner

	switch pathType {
	case "file":
		file, err := os.Open(path)
		defer file.Close()
		if err != nil {
			result.err = err
			return result
		}
		scaner = bufio.NewScanner(file)
	case "url":
		resp, err := http.Get(path)
		defer resp.Body.Close()
		if err != nil {
			result.err = err
			return result
		}
		if resp.StatusCode != http.StatusOK {
			result.err = fmt.Errorf("path: %s response: %s", path, resp.Status)
			return result
		}
		scaner = bufio.NewScanner(resp.Body)
	}
	for scaner.Scan() {
		result.count += strings.Count(scaner.Text(), "Go")
	}
	return result
}
