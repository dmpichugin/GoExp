package ip2string

import (
	"bytes"
	"strconv"
)

func base10ip2string(ip uint32) string {
	var buf bytes.Buffer

	var bytesArray [4]uint32
	for i := 0; i < 4; i++ {
		bytesArray[i] = ip & 255
		ip = ip >> 8
	}

	l := len(bytesArray) - 1
	for i := l; i >= 0; i-- {
		str := strconv.Itoa(int(bytesArray[i]))
		buf.WriteString(str)
		if i != 0 {
			buf.WriteRune('.')
		}
	}

	return buf.String()
}
