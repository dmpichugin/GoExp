package sortmerge

func SortMerge(a []int) []int {
	if len(a) <= 1 {
		return a
	}
	mid := len(a) / 2
	l := a[:mid]
	r := a[mid:]

	l = SortMerge(l)
	r = SortMerge(r)

	return mrg(l, r)
}

func mrg(l, r []int) []int {

	var result []int

	for len(l) > 0 || len(r) > 0 {
		if len(l) > 0 && len(r) > 0 {
			if l[0] <= r[0] {
				result = append(result, l[0])
				l = l[1:]
			} else {
				result = append(result, r[0])
				r = r[1:]
			}
		} else if len(l) > 0 {
			result = append(result, l[0])
			l = l[1:]
		} else if len(r) > 0 {
			result = append(result, r[0])
			r = r[1:]
		}
	}
	return result
}
