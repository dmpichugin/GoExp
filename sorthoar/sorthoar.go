package sorthoar

func SortHoar(a []int, l int, r int) {
	if l > r {
		return
	}
	left := l
	right := r
	pivot := a[(left+right)/2]
	for left <= right {
		for a[right] > pivot {
			right--
		}
		for a[left] < pivot {
			left++
		}
		if left <= right {
			a[right], a[left] = a[left], a[right]
			left++
			right--
		}
	}

	if right != l {
		SortHoar(a, l, right)
	}
	if left != r {
		SortHoar(a, left, r)
	}
}
