package sortbuble

// SortBuble sorts slice integer numbers by bubble method
func SortBuble(a []int) []int {
	for i := 0; i < len(a)-1; i++ {
		for j := 0; j < len(a)-1; j++ {
			if a[j] > a[j+1] {
				a[j], a[j+1] = a[j+1], a[j]
			}
		}
	}
	return a
}
