package main

import (
	"encoding/json"
	"fmt"
	"github.com/labstack/echo"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
)

func APIGetUser(c echo.Context) error {
	UserID := c.Param("id")
	if UserID == "" {
		return c.NoContent(http.StatusNotFound)
	}
	id, err := strconv.Atoi(UserID)
	if err != nil {
		return c.NoContent(http.StatusNotFound)
	}
	u, err := DBGetUserByID(id)
	if err != nil {
		return c.NoContent(http.StatusNotFound)
	}
	return c.JSON(http.StatusOK, u)
}

func APIGetLocation(c echo.Context) error {
	LocationId := c.Param("id")
	if LocationId == "" {
		return c.NoContent(http.StatusNotFound)
	}
	id, err := strconv.Atoi(LocationId)
	if err != nil {
		return c.NoContent(http.StatusNotFound)
	}
	l, err := DBGetLocationByID(id)
	if err != nil {
		return c.NoContent(http.StatusNotFound)
	}
	return c.JSON(http.StatusOK, l)
}

func APIGetVisit(c echo.Context) error {
	VisitID := c.Param("id")
	if VisitID == "" {
		return c.NoContent(http.StatusNotFound)
	}
	id, err := strconv.Atoi(VisitID)
	if err != nil {
		return c.NoContent(http.StatusNotFound)
	}
	v, err := DBGetVisitById(id)
	if err != nil {
		return c.NoContent(http.StatusNotFound)
	}
	return c.JSON(http.StatusOK, v)
}

func APIGetUserVisits(c echo.Context) error {
	var (
		id         int
		fromDate   int
		country    string
		toDate     int
		toDistance int
		err        error
	)

	formParams, err := c.FormParams()
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}

	UserID := c.Param("id")
	id, err = strconv.Atoi(UserID)
	if err != nil {
		fmt.Println("Bad id")
		return c.NoContent(http.StatusNotFound)
	}

	if len(formParams) != 0 {
		// Проверяем случай, когда поля определены, но они пустые.
		for k, _ := range formParams {
			if formParams.Get(k) == "" {
				return c.NoContent(http.StatusBadRequest)
			}
		}
		// Конвертим данные
		if formParams.Get("fromDate") != "" {
			fromDate, err = strconv.Atoi(formParams.Get("fromDate"))
			if err != nil {
				return c.NoContent(http.StatusBadRequest)
			}
		}
		fmt.Println("toDate: ", formParams.Get("toDate"))
		if formParams.Get("toDate") != "" {
			toDate, err = strconv.Atoi(formParams.Get("toDate"))
			if err != nil {
				return c.NoContent(http.StatusBadRequest)
			}
		}
		if formParams.Get("country") != "" {
			country = formParams.Get("country")
		}
		if formParams.Get("toDistance") != "" {
			toDistance, err = strconv.Atoi(formParams.Get("toDistance"))
			if err != nil {
				return c.NoContent(http.StatusBadRequest)
			}
		}
	}

	result, err := DBGetUserVisits(id, fromDate, toDate, country, toDistance)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}

	return c.JSON(http.StatusOK, result)
}

func APIGetAvgMarkLocation(c echo.Context) error {
	var (
		id       int
		fromDate int
		toDate   int
		fromAge  int
		toAge    int
		gender   string
		err      error
	)

	formParams, err := c.FormParams()
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}

	locationId := c.Param("id")
	id, err = strconv.Atoi(locationId)
	if err != nil || id < 0 {
		return c.NoContent(http.StatusNotFound)
	}

	if len(formParams) != 0 {
		// Проверяем случай, когда поля определены, но они пустые.
		for k, _ := range formParams {
			if formParams.Get(k) == "" {
				return c.NoContent(http.StatusBadRequest)
			}
		}
		// Конвертим данные
		if formParams.Get("fromDate") != "" {
			fromDate, err = strconv.Atoi(formParams.Get("fromDate"))
			if err != nil {
				return c.NoContent(http.StatusBadRequest)
			}
		}
		fmt.Println("toDate: ", formParams.Get("toDate"))
		if formParams.Get("toDate") != "" {
			toDate, err = strconv.Atoi(formParams.Get("toDate"))
			if err != nil {
				return c.NoContent(http.StatusBadRequest)
			}
		}
		if formParams.Get("gender") != "" {
			gender = formParams.Get("gender")
		}
		if formParams.Get("fromAge") != "" {
			fromAge, err = strconv.Atoi(formParams.Get("fromAge"))
			if err != nil {
				return c.NoContent(http.StatusBadRequest)
			}
		}
		if formParams.Get("toAge") != "" {
			toAge, err = strconv.Atoi(formParams.Get("toAge"))
			if err != nil {
				return c.NoContent(http.StatusBadRequest)
			}
		}
	}

	am, err := DBGetAvgMarkLocation(id, fromDate, toDate, fromAge, toAge, gender)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	return c.JSON(http.StatusOK, am)
}

func APIUpdateUser(c echo.Context) error {
	var err error
	user := new(User)

	idReq := c.Param("id")
	id, err := strconv.Atoi(idReq)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}

	// Вычитывае body запроса для проверки на поля c null
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return c.NoContent(http.StatusNotFound)
	}
	re := regexp.MustCompile(`:\s*null`)
	res := re.Match(body)
	if res {
		return c.NoContent(http.StatusBadRequest)
	}

	err = json.Unmarshal(body, &user)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	user.ID = id

	ra, err := DBUpdateUser(user)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}

	if ra == 0 {
		return c.NoContent(http.StatusNotFound)
	}

	return c.JSON(http.StatusOK, struct{}{})
}

func APIUpdateVisit(c echo.Context) error {
	var err error
	visit := new(Visit)

	idReq := c.Param("id")
	id, err := strconv.Atoi(idReq)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	// Вычитывае body запроса для проверки на поля c null
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return c.NoContent(http.StatusNotFound)
	}

	re := regexp.MustCompile(`:\s*null`)
	res := re.Match(body)
	if res {
		return c.NoContent(http.StatusBadRequest)
	}

	err = json.Unmarshal(body, &visit)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	visit.Id = id

	ra, err := DBUpdateVisit(visit)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}

	if ra == 0 {
		return c.NoContent(http.StatusNotFound)
	}

	return c.JSON(http.StatusOK, struct{}{})
}

func APIUpdateLocations(c echo.Context) error {
	var err error
	location := new(Location)

	idReq := c.Param("id")
	id, err := strconv.Atoi(idReq)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}

	// Вычитывае body запроса для проверки на поля c null
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return c.NoContent(http.StatusNotFound)
	}

	re := regexp.MustCompile(`:\s*null`)
	res := re.Match(body)
	if res {
		return c.NoContent(http.StatusBadRequest)
	}

	err = json.Unmarshal(body, &location)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	location.Id = id

	ra, err := DBUpdateLocation(location)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}

	if ra == 0 {
		return c.NoContent(http.StatusNotFound)
	}

	return c.JSON(http.StatusOK, struct{}{})
}

func APIAddUser(c echo.Context) error {
	var err error
	user := new(User)
	// Вычитывае body запроса для проверки на поля c null
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return c.NoContent(http.StatusNotFound)
	}
	re := regexp.MustCompile(`:\s*null`)
	res := re.Match(body)
	if res {
		return c.NoContent(http.StatusBadRequest)
	}

	err = json.Unmarshal(body, &user)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}

	err = DBAddUser(user)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	return c.JSON(http.StatusOK, struct{}{})
}

func APIAddVisit(c echo.Context) error {
	var err error
	visit := new(Visit)

	// Вычитывае body запроса для проверки на поля c null
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return c.NoContent(http.StatusNotFound)
	}
	re := regexp.MustCompile(`:\s*null`)
	res := re.Match(body)
	if res {
		return c.NoContent(http.StatusBadRequest)
	}

	err = json.Unmarshal(body, &visit)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}

	err = DBAddVisit(visit)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	return c.JSON(http.StatusOK, struct{}{})
}

func APIAddLocation(c echo.Context) error {
	var err error
	location := new(Location)

	// Вычитывае body запроса для проверки на поля c null
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return c.NoContent(http.StatusNotFound)
	}
	re := regexp.MustCompile(`:\s*null`)
	res := re.Match(body)
	if res {
		return c.NoContent(http.StatusBadRequest)
	}

	err = json.Unmarshal(body, &location)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}

	err = DBAddLocation(location)
	if err != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	return c.JSON(http.StatusOK, struct{}{})
}

// Заглушка
func APIErr(c echo.Context) error {
	return c.NoContent(http.StatusNotFound)
}
