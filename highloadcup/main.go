package main

import (
	"database/sql"
	"flag"
	"fmt"
	"github.com/labstack/echo"
	_ "github.com/lib/pq"
	"log"
)

var db *sql.DB

const (
	user       = "postgres"
	password   = "1234"
	bd         = "highloadcup"
	ssldisable = "?sslmode=disable"
)

func main() {

	modePtr := flag.String("mode", "work", "If you want load data launch program with flag 'loaddata'")

	flag.Parse()
	var err error
	db, err = sql.Open("postgres", fmt.Sprintf("postgres://%s:%s@localhost/%s%s", user, password, bd, ssldisable))
	if err != nil {
		log.Fatal(err)
	}
	if err = db.Ping(); err != nil {
		log.Fatal(err)
	}
	// Заливаем данные
	if *modePtr == "loaddata" {
		LoadData()
		fmt.Println("\n\nData loaded")
		return
	}

	e := echo.New()
	g := e.Group("/api")

	g.GET("/users/:id", APIGetUser)
	g.GET("/visits/:id", APIGetVisit)
	g.GET("/locations/:id", APIGetLocation)
	g.GET("/users/:id/visits", APIGetUserVisits)
	g.GET("/locations/:id/avg", APIGetAvgMarkLocation)

	g.POST("/users/:id", APIUpdateUser)
	g.POST("/visits/:id", APIUpdateVisit)
	g.POST("/locations/:id", APIUpdateLocations)

	g.POST("/users/new", APIAddUser)
	g.POST("/visits/new", APIAddVisit)
	g.POST("/locations/new", APIAddLocation)

	// Заглушки
	g.GET("/*", APIErr)
	g.POST("/*", APIErr)

	log.Fatal(e.Start(":7777"))
}
