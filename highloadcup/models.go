package main

type (
	User struct {
		ID        int    `json:"id"`
		Email     string `json:"email"`
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
		Gender    string `json:"gender"`
		BirthDate int    `json:"birth_date"`
	}

	Visit struct {
		Id        int `json:"id"`
		Location  int `json:"location"`
		User      int `json:"user"`
		VisitedAt int `json:"visited_at"`
		Mark      int `json:"mark"`
	}

	Location struct {
		Id       int    `json:"id"`
		Place    string `json:"place"`
		Country  string `json:"country"`
		City     string `json:"city"`
		Distance int    `json:"distance"`
	}

	VisitAnswer struct {
		Mark      int    `json:"mark"`
		VisitedAt int    `json:"visited_at"`
		Place     string `json:"place"`
	}

	AvgMark struct {
		Avg float32 `json:"avg"`
	}

	ArrayLocations struct {
		Locations []Location
	}

	ArrayUsers struct {
		Users []User
	}

	ArrayVisits struct {
		Visits []Visit
	}

	ArrayVisitsAnswer struct {
		VAnswers []VisitAnswer `json:"visits"`
	}
)
