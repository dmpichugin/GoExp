Данные можно взять [отсюда](https://github.com/sat2707/hlcupdocs) и положить в папку `data` в формате users\*.json и тд. Схему БД можно взять из hl.backup.

Чтобы запустить:
```
dep ensure
go build -o hl
./hl -mode=loaddata
./hl
```