package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

// LoadData ищет файлы с данными в папке data,
// в соответствии с префиксом файлы передаются функциям обработчикам
func LoadData() {
	fmt.Println("Data load in db:")
	files, err := ioutil.ReadDir("./data")
	if err != nil {
		log.Fatal(err)
	}
	for _, file := range files {
		if strings.HasPrefix(file.Name(), "users") {
			err = LoadFromFileUsers(file.Name())
			if err != nil {
				panic(err)
			}
		} else if strings.HasPrefix(file.Name(), "visits") {
			err = LoadFromFileVisits(file.Name())
			if err != nil {
				panic(err)
			}
		} else if strings.HasPrefix(file.Name(), "locations") {
			err = LoadFromFileLocations(file.Name())
			if err != nil {
				panic(err)
			}
		}
	}
}

// LoadFromFileLocations считывает файл с location в память.
// Преобразует из JSON в массив струтур
// и передает функции обработчку для загрузки в БД
func LoadFromFileLocations(filename string) error {
	data, err := ioutil.ReadFile(fmt.Sprintf("./data/%s", filename))
	if err != nil {
		fmt.Println(err)
		return err
	}
	var slice ArrayLocations
	err = json.Unmarshal(data, &slice)
	if err != nil {
		fmt.Println(err)
		return err
	}
	err = LoadLocationsInDB(&slice, filename)
	if err != nil {
		panic(err)
	}
	return nil
}

// LoadLocationsInDB заливает в БД данные из массива с location
func LoadLocationsInDB(a *ArrayLocations, s string) error {
	var userarray [5]Location
	var k int
	var v Location
	stm, err := db.Prepare("INSERT INTO locations VALUES($1, $2, $3,$4,$5),($6, $7,$8,$9,$10),($11, $12, $13,$14,$15),($16, $17, $18,$19,$20),($21, $22, $23,$24,$25)")
	if err != nil {
		fmt.Println(err)
		return err
	}
	// Вставляем пачками по 5 штук в бд
	for k, v = range a.Locations {
		userarray[k%5] = v
		if k%5 == 4 {
			_, err = stm.Exec(userarray[0].Id, userarray[0].Place, userarray[0].Country, userarray[0].City, userarray[0].Distance,
				userarray[1].Id, userarray[1].Place, userarray[1].Country, userarray[1].City, userarray[1].Distance,
				userarray[2].Id, userarray[2].Place, userarray[2].Country, userarray[2].City, userarray[2].Distance,
				userarray[3].Id, userarray[3].Place, userarray[3].Country, userarray[3].City, userarray[3].Distance,
				userarray[4].Id, userarray[4].Place, userarray[4].Country, userarray[4].City, userarray[4].Distance,
			)
			if err != nil {
				fmt.Println(err)
				return err
			}
		}
	}
	// Вставляем оставшиеся (если кол-во не кратно 5)
	j := len(a.Locations) % 5
	if j != 0 {
		for i := 0; i < j; i++ {
			_, err = db.Exec("INSERT INTO locations VALUES($1, $2, $3, $4, $5)", userarray[i].Id, userarray[i].Place, userarray[i].Country, userarray[i].City, userarray[i].Distance)
			if err != nil {
				fmt.Println(err)
				return err
			}
		}
	}
	log.Println(s, ": Load location end")
	return nil
}

// LoadFromFileUsers считывает файл с users в память.
// Преобразует из JSON в массив струтур
// и передает функции обработчку для загрузки в БД
func LoadFromFileUsers(filename string) error {
	data, err := ioutil.ReadFile(fmt.Sprintf("./data/%s", filename))
	if err != nil {
		fmt.Println(err)
		return err
	}
	var slice ArrayUsers
	err = json.Unmarshal(data, &slice)
	if err != nil {
		fmt.Println(err)
		return err
	}
	err = LoadUsersInDB(&slice, filename)
	if err != nil {
		panic(err)
	}
	return nil
}

// LoadUsersInDB заливает в БД данные из массива с users
func LoadUsersInDB(a *ArrayUsers, s string) error {
	var (
		userarray [5]User
		k         int
		v         User
		err       error
	)
	stm, err := db.Prepare("INSERT INTO users VALUES($1, $2, $3,$4,$5,$6),($7,$8,$9,$10,$11,$12),($13, $14, $15,$16,$17,$18),($19, $20, $21,$22,$23,$24),($25, $26, $27,$28,$29,$30)")
	if err != nil {
		fmt.Println(err)
		return err
	}
	// Вставляем пачками по 5 штук в бд
	for k, v = range a.Users {
		userarray[k%5] = v
		if k%5 == 4 {
			_, err = stm.Exec(userarray[0].ID, userarray[0].Email, userarray[0].FirstName, userarray[0].LastName, userarray[0].Gender, userarray[0].BirthDate,
				userarray[1].ID, userarray[1].Email, userarray[1].FirstName, userarray[1].LastName, userarray[1].Gender, userarray[1].BirthDate,
				userarray[2].ID, userarray[2].Email, userarray[2].FirstName, userarray[2].LastName, userarray[2].Gender, userarray[2].BirthDate,
				userarray[3].ID, userarray[3].Email, userarray[3].FirstName, userarray[3].LastName, userarray[3].Gender, userarray[3].BirthDate,
				userarray[4].ID, userarray[4].Email, userarray[4].FirstName, userarray[4].LastName, userarray[4].Gender, userarray[4].BirthDate,
			)
			if err != nil {
				fmt.Println(err)
				return err
			}
		}
	}
	// Вставляем оставшиеся (если кол-во не кратно 5)
	j := len(a.Users) % 5
	if j != 0 {
		for i := 0; i < j; i++ {
			_, err = db.Exec("INSERT INTO users VALUES($1, $2, $3, $4, $5,$6)", userarray[i].ID, userarray[i].Email, userarray[i].FirstName, userarray[i].LastName, userarray[i].Gender, userarray[i].BirthDate)
			if err != nil {
				fmt.Println(err)
				return err
			}
		}
	}
	log.Println(s, ": Load user end")
	return nil
}

// LoadFromFileVisits считывает файл с visits в память.
// Преобразует из JSON в массив струтур
// и передает функции обработчку для загрузки в БД
func LoadFromFileVisits(filename string) error {
	data, err := ioutil.ReadFile(fmt.Sprintf("./data/%s", filename))
	if err != nil {
		fmt.Println(err)
		return err
	}
	var slice ArrayVisits
	err = json.Unmarshal(data, &slice)
	if err != nil {
		fmt.Println(err)
		return err
	}
	err = LoadVisitsInDB(&slice, filename)
	if err != nil {
		panic(err)
	}
	return nil
}

// LoadVisitsInDB заливает в БД данные из массива с visits
func LoadVisitsInDB(a *ArrayVisits, s string) error {
	var (
		userarray [5]Visit
		k         int
		v         Visit
		err       error
	)
	stm, err := db.Prepare("INSERT INTO visits VALUES($1, $2, $3,$4,$5),($6, $7, $8,$9,$10),($11, $12, $13,$14,$15),($16, $17, $18,$19,$20),($21, $22, $23,$24,$25)")
	if err != nil {
		fmt.Println(err)
		return err
	}
	// Вставляем пачками по 5 штук в бд
	for k, v = range a.Visits {
		userarray[k%5] = v
		if k%5 == 4 {
			_, err = stm.Exec(userarray[0].Id, userarray[0].Location, userarray[0].User, userarray[0].VisitedAt, userarray[0].Mark,
				userarray[1].Id, userarray[1].Location, userarray[1].User, userarray[1].VisitedAt, userarray[1].Mark,
				userarray[2].Id, userarray[2].Location, userarray[2].User, userarray[2].VisitedAt, userarray[2].Mark,
				userarray[3].Id, userarray[3].Location, userarray[3].User, userarray[3].VisitedAt, userarray[3].Mark,
				userarray[4].Id, userarray[4].Location, userarray[4].User, userarray[4].VisitedAt, userarray[4].Mark)
			if err != nil {
				fmt.Println(err)
				return err
			}
		}
	}
	// Вставляем оставшиеся (если кол-во не кратно 5)
	j := len(a.Visits) % 5
	if j != 0 {
		for i := 0; i < j; i++ {
			_, err = db.Exec("INSERT INTO visits VALUES($1, $2, $3, $4, $5)", userarray[i].Id, userarray[i].Location, userarray[i].User, userarray[i].VisitedAt, userarray[i].Mark)
			if err != nil {
				fmt.Println(err)
				return err
			}
		}
	}
	log.Println(s, ": Load visit end")
	return nil
}
