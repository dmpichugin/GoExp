package main

import (
	"database/sql"
	sq "github.com/Masterminds/squirrel"
	_ "github.com/lib/pq"
	"log"
)

func DBGetUserByID(i int) (*User, error) {
	row := db.QueryRow("SELECT * FROM users WHERE id = $1", i)
	u := &User{}
	err := row.Scan(&u.ID, &u.Email, &u.FirstName, &u.LastName, &u.Gender, &u.BirthDate)
	if err == sql.ErrNoRows {
		return nil, err
	}
	return u, nil
}

func DBGetLocationByID(i int) (*Location, error) {
	row := db.QueryRow("SELECT * FROM locations WHERE id = $1", i)
	l := &Location{}
	err := row.Scan(&l.Id, &l.Place, &l.Country, &l.City, &l.Distance)
	if err == sql.ErrNoRows {
		return nil, err
	}
	return l, nil
}

func DBGetVisitById(i int) (*Visit, error) {
	row := db.QueryRow("SELECT * FROM visits WHERE id = $1", i)
	v := &Visit{}
	err := row.Scan(&v.Id, &v.Location, &v.User, &v.VisitedAt, &v.Mark)
	if err == sql.ErrNoRows {
		return nil, err
	}
	return v, nil
}

func DBGetUserVisits(id int, fromDate int, toDate int, country string, tDist int) (*ArrayVisitsAnswer, error) {
	var err error
	dbCache := sq.NewStmtCacher(db)
	mydb := sq.StatementBuilder.RunWith(dbCache).PlaceholderFormat(sq.Dollar)

	// Кол-во полей может быть разным, поэтому запрос собирается в зависимости от значений полей
	s := mydb.Select("mark, visitedat, place").From("visits inner join locations on visits.location=locations.id").Where("visits.user_id=?", id)
	if fromDate != 0 {
		s = s.Where("visitedat>?", fromDate)
	}
	if toDate != 0 {
		s = s.Where("visitedat<?", toDate)
	}
	if country != "" {
		s = s.Where("country=?", country)
	}
	if tDist != 0 {
		s = s.Where("distance<?", tDist)
	}
	vs := new(ArrayVisitsAnswer)
	rows, err := s.Query()
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var vk VisitAnswer
	for rows.Next() {
		err = rows.Scan(&vk.Mark, &vk.VisitedAt, &vk.Place)
		if err != nil {
			log.Fatal(err)
		}
		vs.VAnswers = append(vs.VAnswers, vk)
	}
	if err = rows.Err(); err != nil {
		log.Fatal(err)
	}
	return vs, nil
}

func DBGetAvgMarkLocation(id, fromDate, toDate, fromAge, toAge int, gender string) (*AvgMark, error) {
	dbCache := sq.NewStmtCacher(db)
	mydb := sq.StatementBuilder.RunWith(dbCache).PlaceholderFormat(sq.Dollar)
	// Кол-во полей может быть разным, поэтому запрос собирается в зависимости от значений полей
	s := mydb.Select("round(avg(mark),2)").From("visits inner join users on visits.user_id=users.id").Where("visits.location=?", id)
	if fromDate != 0 {
		s = s.Where("visitedat>?", fromDate)
	}
	if toDate != 0 {
		s = s.Where("visitedat<?", toDate)
	}
	if gender != "" {
		s = s.Where("users.gender=?", gender)
	}
	if fromAge != 0 {
		s = s.Where("users.birthdate>?", fromAge)
	}
	if toAge != 0 {
		s = s.Where("users.birthdate<?", toAge)
	}
	s = s.GroupBy("location")

	row := s.QueryRow()
	am := new(AvgMark)

	err := row.Scan(&am.Avg)
	if err == sql.ErrNoRows {
		return nil, err
	}

	return am, nil
}

func DBUpdateUser(u *User) (int, error) {
	var err error
	dbCache := sq.NewStmtCacher(db)
	mydb := sq.StatementBuilder.RunWith(dbCache).PlaceholderFormat(sq.Dollar)
	// Кол-во полей может быть разным, поэтому запрос собирается в зависимости от значений полей
	s := mydb.Update("Users").Where("id=?", u.ID)
	if u.Email != "" {
		s = s.Set("email", u.Email)
	}
	if u.FirstName != "" {
		s = s.Set("firstname", u.FirstName)
	}
	if u.LastName != "" {
		s = s.Set("lastname", u.LastName)
	}
	if u.BirthDate != 0 {
		s = s.Set("birthdate", u.BirthDate)
	}
	if u.Gender != "" {
		s = s.Set("gender", u.Gender)
	}
	result, err := s.Exec()
	if err != nil {
		return 0, err
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return int(rowsAffected), nil
}

func DBUpdateVisit(v *Visit) (int, error) {
	var err error
	dbCache := sq.NewStmtCacher(db)
	mydb := sq.StatementBuilder.RunWith(dbCache).PlaceholderFormat(sq.Dollar)
	// Кол-во полей может быть разным, поэтому запрос собирается в зависимости от значений полей
	s := mydb.Update("Visits").Where("id=?", v.Id)
	if v.Location != 0 {
		s = s.Set("location", v.Location)
	}
	if v.Mark != 0 {
		s = s.Set("mark", v.Mark)
	}
	if v.User != 0 {
		s = s.Set("user_id", v.User)
	}
	if v.VisitedAt != 0 {
		s = s.Set("visitedat", v.VisitedAt)
	}
	result, err := s.Exec()
	if err != nil {
		return 0, err
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return int(rowsAffected), nil
}

func DBUpdateLocation(l *Location) (int, error) {
	var err error
	dbCache := sq.NewStmtCacher(db)
	mydb := sq.StatementBuilder.RunWith(dbCache).PlaceholderFormat(sq.Dollar)
	// Кол-во полей может быть разным, поэтому запрос собирается в зависимости от значений полей
	s := mydb.Update("Locations").Where("id=?", l.Id)
	if l.Place != "" {
		s = s.Set("place", l.Place)
	}
	if l.City != "" {
		s = s.Set("city", l.City)
	}
	if l.Country != "" {
		s = s.Set("country", l.Country)
	}
	if l.Distance != 0 {
		s = s.Set("distance", l.Distance)
	}
	result, err := s.Exec()
	if err != nil {
		return 0, err
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return int(rowsAffected), nil
}

func DBAddUser(u *User) error {
	_, err := db.Exec("INSERT INTO users VALUES($1,$2,$3,$4,$5,$6)", u.ID, u.Email, u.FirstName, u.LastName, u.Gender, u.BirthDate)
	return err
}

func DBAddVisit(v *Visit) error {
	_, err := db.Exec("INSERT INTO visits VALUES($1,$2,$3,$4,$5)", v.Id, v.Location, v.User, v.VisitedAt, v.Mark)
	return err
}

func DBAddLocation(l *Location) error {
	_, err := db.Exec("INSERT INTO locations VALUES($1,$2,$3,$4,$5)", l.Id, l.Place, l.Country, l.City, l.Distance)
	return err
}
